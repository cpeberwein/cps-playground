const path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');


module.exports = (env, argv) => {
  return {
    mode: 'production',
    entry: {
      'app': './assets/JavaScript/app.js',
    },
    output: {
      path: path.resolve(__dirname, 'packages/site-distribution/Resources/Public/'),
      filename: 'JavaScript/[name].js',
      publicPath: '../'
    },
    watchOptions: {
      ignored: ['node_modules']
    },
    plugins: [
      new CopyWebpackPlugin({
        patterns: [
          // Define patterns of files you want to copy
          // to your site-distributions "Public" folder
          { from: 'assets/Image', to: './Image' }
        ]
      }),
      new SpriteLoaderPlugin({
        plainSprite: true,
      }),
    ],
    module: {
      rules: [
        {
          test: /\.(png|jpe?g|gif|ttf|woff2|otf)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: 'Font/',
              }
            }
          ]
        },
        {
          test: /\.s[ac]ss$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].css',
                outputPath: './Css/'
              }
            },
            { loader: 'extract-loader' },
            { loader: 'css-loader' },
            {
              loader: 'postcss-loader',
              options: {
                postcssOptions: {
                  plugins: [
                    [
                      "postcss-preset-env", {},
                      "autoprefixer", {},
                    ],
                  ],
                },
              },
            },
            {
              loader: 'sass-loader',
              options: {
                additionalData: "$mode: " + argv.mode + ";",
              }
            },
          ]
        },
        {
          test: /.*\.svg$/,
          loader: 'svg-sprite-loader',
          options: {
            extract: true,
            spriteFilename: 'Image/Sprite/icons.svg',
            runtimeCompat: true
          }
        }
      ]
    },
  }
};
